'use strict';

angular.module('dFilm-Test', ['dFilm', 'ngMockE2E', 'ngCookies'])
  .config(function($stateProvider, $locationProvider) {
    $locationProvider.html5Mode(false);

    $stateProvider
      .state('authenticate', {
        url: "/authenticate",
        template: '<div><label>Username</label><input name="username" type="text" ng-model="username" /><br/><button id="submit" ng-click="auth(username)">AUTHENTICATE</button></div>',
        controller: function($scope, $window, $cookies, $http) {
          $scope.auth = function(username) {
            $http.post('/db/data/node', {
              username: username,
              oauthAccessToken: 'oauthAccessToken',
              authExpires: 'authExpires',
              evernoteId: 'evernoteId'
            })
            .success(function() {
              $cookies['dfilm_session'] = 'exp=1401591720453&data='+JSON.stringify({id: "1", username: username})+'&digest=d8ffd3abc65c7d328754adf941c8e1ec545a2b0c';
              $window.location.href = '/';
            });
          };
        }
      });
  })
  .run(function($httpBackend) {
    /*
    Films = {imageWidth: '180', imageHeight: '270'}
    Books = {imageWidth: '180', imageHeight: '270'}
    Articles = {imageWidth: '370', imageHeight: '270'}
    Notes = {imageWidth: '370', imageHeight: '130'}
    People = {imageWidth: '180', imageHeight: '270'}
    Misc = {imageWidth: '180', imageHeight: '130'}
     */

    $httpBackend.whenGET().passThrough();
    $httpBackend.whenDELETE().passThrough();
    $httpBackend.whenJSONP().passThrough();
    $httpBackend.whenPOST().passThrough();
    $httpBackend.whenPUT().passThrough();
  });