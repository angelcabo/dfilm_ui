'use strict';

angular.module('dFilm.services', ['ngResource']);
angular.module('dFilm.controllers', []);
angular.module('dFilm.directives', []);
angular.module('dFilm', ['ngCookies', 'ui', 'ui.state', 'ui.bootstrap', 'dFilm.services', 'dFilm.controllers', 'dFilm.directives'])
  .config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise("/");

    $stateProvider
      .state('/main', {
        url: "/",
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .state('artifacts', {
        url: "/artifacts",
        abstract: true,
        templateUrl: 'views/artifacts.html',
        controller: 'ArtifactCtrl',
        resolve: {
          artifacts : ['User', function(User) {
            return User.getArtifacts();
          }],
          projects : ['User', function(User) {
            return User.projects.query({userId: User.id()});
          }]
        }
      })
        .state('artifacts.index', {
          url: ""
        })
        .state('artifacts.project', {
          url: "/project/:projectId"
        })
      .state('artifact', {
        url: '/artifacts/:artifactId',
        templateUrl: 'views/artifact_show.html',
        controller: 'ArtifactShowCtrl',
        resolve: {
          artifact : ['User', '$stateParams', function(User, $stateParams) {
            return User.getArtifact($stateParams.artifactId);
          }]
        }
      })
      .state('/projectList', {
        url: '/projects',
        templateUrl: 'views/projects.html',
        controller: 'ProjectsCtrl',
        resolve: {
          projects : ['User', function(User) {
            return User.projects.query({userId: User.id()});
          }]
        }
      })
      .state('project', {
        url: '/projects/:projectId',
        templateUrl: 'views/artifacts.html',
        controller: 'ArtifactCtrl',
        resolve: {
          artifacts : ['User', function(User) {
            return User.artifacts.query({userId: User.id()});
          }],
          projects : ['User', function(User) {
            return User.projects.query({userId: User.id()});
          }]
        }
      });
  })
  .run(function($rootScope, $state, $cookies, $location, User) {
    var defaultLayouts = {
      header: 'views/navbar.html',
      sidebar: 'views/sidebar.html'
    };

    $rootScope.layoutPartial = function(partialName) {
      return $state.current[partialName] || defaultLayouts[partialName];
    };

    User.setSession($cookies['dfilm_session']);
    $rootScope.User = User;

    $rootScope.$watch(function() { return $location.path(); }, function(newValue){
      if (User.isAuthenticated() == false && newValue != '/authenticate'){
        $location.path('/');
      }
    });
  });
