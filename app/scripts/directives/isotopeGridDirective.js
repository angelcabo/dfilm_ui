'use strict';

angular.module('dFilm.directives')
  .directive('isotopeGrid', function ($timeout, $compile) {

    var _computeFilterStr = function(filter) {
      var filterPrepend = filter.project ? ".project-" + filter.project : "";
      if (filter.artifacts.length > 0) {
        return filterPrepend + $.map(filter.artifacts, function(artifactType) {
          return artifactType + (filter.relationshipTypes || []).join();
        }).join(', ');
      } else {
        return filterPrepend + (filter.relationshipTypes || []).join(', ');
      }
    };

    var _artifactClassSelector = function(artifact) {
      var defaultSelector = "artifact " + artifact.type;
      angular.forEach(artifact.projects, function(project){
        defaultSelector = defaultSelector.concat(" project-" + project.id);
      });
      angular.forEach(artifact.relationshipType, function(type){
        defaultSelector = defaultSelector.concat(" " + type.toLowerCase());
      });
      return defaultSelector;
    };

    return {
      link: function (scope, element, attrs) {
        var collection = scope.$eval(attrs.isotopeGrid);

        var filterArtifacts = function() {
          var filterStr = _computeFilterStr(scope.filter);
          element.isotope({ filter: filterStr });
        };

        var buildArtifact = function(artifact) {
          var classSel = _artifactClassSelector(artifact);
          var template =
            '<div id="artifact-' + collection.indexOf(artifact) + '" data-ng-click="showArtifact(' + artifact.id + ')" class="' + classSel + '">' +
              '<img data-ng-src="' + artifact.imageUrl + '" class="artifact-img ' + artifact.type + '-img" width="' + artifact.imageWidth + '" height="' + artifact.imageHeight + '" alt=""/>' +
              '<span class="pull-right">' +
                '<i class="icon-2x icon-remove-sign" data-ng-click="removeArtifact($event, ' + collection.indexOf(artifact) + ')" style="color: white;"></i>' +
              '</span>' +
            '</div>';
          element.isotope('insert', $($compile(template)(scope)));
        };
        
        scope.$on('addItem', function(event, artifact) {
          buildArtifact(artifact);
        });

        scope.$on('removeItem', function(event, index) {
          element.isotope('remove', $('#artifact-' + index));
        });

        $timeout(function() {
          element.imagesLoaded(function () {
            element.isotope({filter: _computeFilterStr(scope.filter)});
            collection.forEach(buildArtifact);
            scope.$watch('filter', filterArtifacts, true);
          });
        });
      }
    };
  });