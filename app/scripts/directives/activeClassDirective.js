'use strict';

angular.module('dFilm.directives')
  .directive('activeClass', function ($location) {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        var path = attrs.activeClass;
        scope.location = $location;
        scope.$watch('location.path()', function (newPath) {
          if (path === newPath) {
            element.addClass('active');
          } else {
            element.removeClass('active');
          }
        });
      }
    };
  });