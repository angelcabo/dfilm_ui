angular.module('dFilm.services')
  .factory('Misc', function(GoogleImageSearch) {
    var miscFormatResult = function (misc) {
      if (misc.notable) {
        return misc.name + ' (' + misc.notable.name + ')';
      } else {
        return misc.name;
      }
    };

    var miscFormatSelection = function (misc) {
      return misc.name;
    };

    return {
      config: {
        minimumInputLength: 3,
        placeholder: 'Search...',
        ajax: {
          url: 'https://www.googleapis.com/freebase/v1/search',
          dataType: 'jsonp',
          data: function (term) {
            return {
              query: term,
              key: 'AIzaSyBgwFoxiVPPP7ik3hkxz1UHIhBjGA47UWw',
              filter: '(all (not type:/people/person ) (not type:/film/film))',
              spell: 'always',
              exact: false,
              prefixed: true
            };
          },
          results: function(data) {
            return {
              results: data.result
            };
          }
        },
        formatResult: miscFormatResult,
        formatSelection: miscFormatSelection
      },
      construct: function (data) {
        return GoogleImageSearch.get(data['name']).then(function(results) {
          return {
            artifact: {
              title: data['name'],
              imageUrl: results.data.responseData.results[0].unescapedUrl,
              imageWidth: '180',
              imageHeight: '130',
              uniqueId: data['id'] || data['mid'],
              type: 'misc'
            }
          };
        });

      }
    }
  });