angular.module('dFilm.services')
  .factory('Book', function ($q) {
    var bookFormatResult = function (book) {
      var result;
      if (book.volumeInfo.authors) {
        result = book.volumeInfo.title + ': ' + book.volumeInfo.subtitle + ' by ' + book.volumeInfo.authors[0] + ' (' + book.volumeInfo.publishedDate + ')';
      } else {
        result = book.volumeInfo.title + ': ' + book.volumeInfo.subtitle + ' (' + book.volumeInfo.publishedDate + ')';
      }
      return result;
    };

    var bookFormatSelection = function (book) {
      return book.volumeInfo.title + ': ' + book.volumeInfo.subtitle;
    };

    return {
      config: {
        minimumInputLength: 3,
        placeholder: 'Enter a title...',
        ajax: {
          url: 'https://www.googleapis.com/books/v1/volumes',
          dataType: 'jsonp',
          data: function (term, page) {
            return {
              q: term
            };
          },
          results: function (data, page) {
            return {
              results: data.items
            };
          }
        },
        formatResult: bookFormatResult,
        formatSelection: bookFormatSelection
      },
      construct: function (book) {
        var deferred = $q.defer();
        var data = {
          artifact: {
            title: book['title'],
            imageUrl: book.volumeInfo.imageLinks.thumbnail,
            imageWidth: '180',
            imageHeight: '270',
            uniqueId: 'tmdb_' + book['id'],
            type: 'book'
          }
        };
        deferred.resolve(data);
        return deferred.promise;
      }
    }
  });