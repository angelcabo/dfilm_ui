angular.module('dFilm.services')
  .factory('Person', function(GoogleImageSearch) {
    var personFormatResult = function (person) {
      return person.name;
    };

    var personFormatSelection = function (person) {
      return person.name;
    };

    return {
      config: {
        minimumInputLength: 3,
        placeholder: 'Enter a name...',
        ajax: {
          url: 'http://api.themoviedb.org/3/search/person',
          dataType: 'jsonp',
          data: function (term) {
            return {
              query: term,
              api_key: '7edb72451916a20e3e9b46a5415ba199'
            };
          },
          results: function(data) {
            return {
              results: data.results
            };
          }
        },
        formatResult: personFormatResult,
        formatSelection: personFormatSelection
      },
      construct: function (person) {
        return GoogleImageSearch.get(person['name']).then(function(results) {
          return {
            artifact: {
              title: person['name'],
              imageUrl: results.data.responseData.results[0].unescapedUrl,
              imageWidth: '180',
              imageHeight: '130',
              uniqueId: 'tmdb_' + person['id'],
              type: 'person'
            }
          };
        });
      }
    }
  });