angular.module('dFilm.services')
  .factory('GoogleImageSearch', function($http) {
    return {
      get: function(query) {
        return $http.jsonp('https://ajax.googleapis.com/ajax/services/search/images?v=1.0&q=' + query + '&callback=JSON_CALLBACK');
      }
    }
  });