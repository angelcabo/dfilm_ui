'use strict';

angular.module('dFilm.services')
  .factory('User', function ($resource, $http) {

    var _session;
    // $http.defaults.headers.common['Cookie'] = 'dfilm_session=' + _session;

    var _parseSession = function(attribute) {
      var attr;
      if (_session) {
        try {
          attr = JSON.parse(_session.split('&')[1].split('=')[1])[attribute];
        } catch(exception) { /* invalid session */ }
      }
      return attr;
    };
    
    // Public API here
    return {
      setSession: function (session) {
        return _session = session;
      },
      isAuthenticated: function () {
        return !!_session;
      },
      name: function () {
        return _parseSession('username');
      },
      id: function () {
        return _parseSession('id');
      },
      addArtifactToProject: function(projectId, artifactId) {
        return $http.put('/api/users/' + this.id() + '/artifacts/' + artifactId + '/add-to-project/' + projectId);
      },
      updateArtifactStatus: function(artifact) {
        if (artifact.type == 'film') {
          return $http.post('/api/users/' + this.id() + '/artifacts/' + artifact.id + '/WATCHED');
        } else if (artifact.type == 'book') {
          return $http.post('/api/users/' + this.id() + '/artifacts/' + artifact.id + '/READ');
        }
      },
      getArtifacts: function() {
        return $http.get('/api/users/' + this.id() + '/artifacts');
      },
      getArtifact: function(artifactId) {
        return $http.get('/api/users/' + this.id() + '/artifacts/' + artifactId);
      },
      projects: $resource('/api/users/:userId/projects/:id', {userId: '@userId', id: '@id'}, {
        saveArtifact: {
          method:'POST',
          params: {userId: '@userId', id: '@id'},
          url: '/api/users/:userId/projects/:id/artifacts'
        }
      }),
      artifacts: $resource('/api/users/:userId/artifacts/:id', {userId: '@userId', id: '@id'}, {
        getRelationships: {
          method:'GET',
          url: '/api/users/:userId/artifacts/:id/relationships',
          isArray: true
        },
        saveRelationship: {
          method:'POST',
          url: '/api/users/:userId/artifacts/:id/relationships'
        },
        updateStatus: {
          method:'POST',
          params: {status: '@status'},
          url: '/api/users/:userId/artifacts/:id/:status'
        }
      })
    };
  });
