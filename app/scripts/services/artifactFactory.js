angular.module('dFilm.services')
  .factory('ArtifactFactory', function(User, Film, Book, Misc, Person){

    var types = {
      Film: Film,
      Book: Book,
      Misc: Misc,
      Person: Person
    };

    return {
      getConfig: function (type) {
        return types[type].config;
      },
      createArtifact: function (type, data, projectId, success) {
        var ArtifactType = types[type],
            artifact;
        return ArtifactType.construct(data)
          .then(function(results) {
            if (projectId) {
              artifact = angular.extend({id: projectId, userId: User.id()}, results);
              User.projects.saveArtifact(artifact, success);
            } else {
              artifact = angular.extend({userId: User.id()}, results);
              User.artifacts.save(artifact, success);
            }
          });
      },
      createRelationship: function (relationship, artifactType, fromArtifact, toArtifact, success) {
        var ArtifactType = types[artifactType];

        return ArtifactType.construct(toArtifact)
          .then(function(results) {
            var relatedArtifact = angular.extend({userId: User.id(), id: fromArtifact.id, relationship: relationship}, results);
            User.artifacts.saveRelationship(relatedArtifact, success);
          });
      }
    };
  });