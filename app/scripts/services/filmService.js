angular.module('dFilm.services')
  .factory('Film', function ($q) {
    var filmFormatResult = function (film) {
      return film.title + ' (' + film.release_date + ')';
    };

    var filmFormatSelection = function (film) {
      return film.title;
    };

    return {
      config: {
        minimumInputLength: 3,
        placeholder: 'Enter a title...',
        ajax: {
          url: 'http://api.themoviedb.org/3/search/movie',
          dataType: 'jsonp',
          data: function (term) {
            return {
              query: term,
              api_key: '7edb72451916a20e3e9b46a5415ba199'
            };
          },
          results: function (data) {
            return {
              results: data.results
            };
          }
        },
        formatResult: filmFormatResult,
        formatSelection: filmFormatSelection
      },
      construct: function (film) {
        var deferred = $q.defer();
        var data = {
          artifact: {
            title: film['title'],
            imageUrl: 'http://cf2.imgobject.com/t/p/original' + film['poster_path'],
            imageWidth: '180',
            imageHeight: '270',
            uniqueId: 'tmdb_' + film['id'],
            type: 'film'
          }
        };
        deferred.resolve(data);
        return deferred.promise;
      }
    }
  });