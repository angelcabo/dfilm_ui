'use strict';

angular.module('dFilm')
  .controller('MainCtrl', function ($scope, $window) {
    $scope.title = 'Discover Film';
    $scope.projectCount = 10;
    $scope.articleCount = 20;
    $scope.filmCount = 30;

    $scope.recentActivity = [
      {title: 'Renoir', projectName: 'Art Films', dateAdded: new Date(), dateWatched: new Date()}
    ];

    $scope.authenticate = function() {
      $window.location.href = $window.location.origin + '/authenticate';
    };

  });
