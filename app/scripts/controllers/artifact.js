'use strict';

angular.module('dFilm')
  .controller('ArtifactCtrl', function ($scope, $location, $state, User, artifacts, projects, ArtifactFactory, $rootScope) {
    $rootScope.lastTenArtifacts = [];
    $scope.artifacts = artifacts.data;
    $scope.projects = projects;
    $scope.typeToAdd = "Film";
    
    $scope.filter = {
      project: "",
      artifacts: [],
      relationshipsTypes: []
    };

    $scope.filterProject = function() {
      if ($scope.filter.project) {
        $location.path('/artifacts/project/' + $scope.filter.project);
      } else {
        $location.path('/artifacts');
      }
    };

    if (angular.isDefined($state.params.projectId)) {
      $scope.filter.project = $state.params.projectId;
    }

    $scope.showArtifact = function(idx) {
      $location.path( '/artifacts/' + idx);
    };
    
    $scope.submitArtifact = function() {
      ArtifactFactory.createArtifact($scope.typeToAdd, this.artifact, $scope.filter.project, function(artifact) {
        if ($.map($scope.artifacts, function(a) { return a["id"]; }).indexOf(artifact.id) < 0) {
          $scope.artifacts.push(artifact);
          $scope.$broadcast('addItem', artifact);
        }
      });
    };

    $scope.select2Config = ArtifactFactory.getConfig('Film');
    $scope.$watch('typeToAdd', function(typeToAdd) {
      if (typeToAdd) {
        $scope.select2Config = ArtifactFactory.getConfig(typeToAdd);
      }
    });

    $scope.toggleFilter = function(filter) {
      var idx = $scope.filter.artifacts.indexOf(filter);
      if (idx >= 0) {
        $scope.filter.artifacts.splice(idx, 1);
      } else {
        $scope.filter.artifacts.push(filter);
      }
    };

    $scope.isActive = function(filter) {
      return $scope.filter.artifacts.indexOf(filter) >= 0 ? 'btn active' : 'btn';
    };

    $scope.removeArtifact = function($event, index) {
      var deleteArtifact = confirm('Are you sure you want to delete ' + $scope.artifacts[index].title + '?');
      if(deleteArtifact) {
        User.artifacts.delete({userId: User.id(), id: $scope.artifacts[index].id});
        $scope.artifacts.splice(index, 1);
        $scope.$broadcast('removeItem', index);
      }
      $event.stopImmediatePropagation();
    }

  });
