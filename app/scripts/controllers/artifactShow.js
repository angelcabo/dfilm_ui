'use strict';

angular.module('dFilm')
  .controller('ArtifactShowCtrl', function ($scope, User, ArtifactFactory, $state, artifact, $location, $rootScope) {
    if (angular.isUndefined($rootScope.lastTenArtifacts)) {
      $rootScope.lastTenArtifacts = [];
    }
    $scope.filter = {
      artifacts: [],
      relationshipTypes: []
    };
    $scope.artifact = artifact.data;
    if ($rootScope.lastTenArtifacts.filter(function(a) {return a.id == $scope.artifact.id}).length === 0) {
      $rootScope.lastTenArtifacts.push($scope.artifact);
    }
    $scope.typeToAdd = "Film";
    $scope.select2Config = ArtifactFactory.getConfig('Film');
    $scope.$watch('typeToAdd', function(typeToAdd) {
      if (typeToAdd) {
        $scope.select2Config = ArtifactFactory.getConfig(typeToAdd);
      }
    });

    $scope.submitRelatedArtifact = function() {
      ArtifactFactory.createRelationship(this.relationship, $scope.typeToAdd, $scope.artifact, this.relatedArtifact, function(artifact) {
        $scope.artifact.relationships.push(artifact);
        $scope.$broadcast('addItem', artifact);
      });
    };

    $scope.showArtifact = function(idx) {
      $location.path( '/artifacts/' + idx);
    };

    $scope.alerts = [];
    $scope.saveStatus = function(artifact) {
      var updateStatus = confirm('Set status update for ' + artifact.title + '?');
      if (updateStatus) {
        User.updateArtifactStatus(artifact).then(function(response) {
          if (response.status == 200) {
            $scope.alerts.push({ type: 'success', msg: "Hope you enjoyed " + artifact.title + "!"});
          } else {
            $scope.alerts.push({ type: 'error', msg: "Sorry, something went wrong."});
          }
        });
      }
    };

    $scope.closeAlert = function(index) {
      $scope.alerts.splice(index, 1);
    };

    $scope.toggleArtifactTypeFilter = function(filter) {
      var idx = $scope.filter.artifacts.indexOf(filter);
      if (idx >= 0) {
        $scope.filter.artifacts.splice(idx, 1);
      } else {
        $scope.filter.artifacts.push(filter);
      }
    };

    $scope.toggleRelTypeFilter = function(filter) {
      var idx = $scope.filter.relationshipTypes.indexOf(filter);
      if (idx >= 0) {
        $scope.filter.relationshipTypes.splice(idx, 1);
      } else {
        $scope.filter.relationshipTypes.push(filter);
      }
    };

    $scope.isActiveRelFilter = function(filter) {
      return $scope.filter.relationshipTypes.indexOf(filter) >= 0 ? 'btn active' : 'btn';
    };
    
    $scope.isActiveArtifactTypeFilter = function(filter) {
      return $scope.filter.artifacts.indexOf(filter) >= 0 ? 'btn active' : 'btn';
    };
  });