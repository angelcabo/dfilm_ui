'use strict';

angular.module('dFilm')
  .controller('ProjectsCtrl', function ($scope, User, projects) {
    $scope.projects = projects;
    $scope.createProject = function(projectData) {
      projectData.active = true;
      var project = User.projects.save(angular.extend({userId: User.id()}, {project: projectData}), function() {
        $scope.projects.push(project);
        $scope.project = {};
      });
    };
  });
