'use strict';

angular.module('dFilm')
  .controller('NavCtrl', function ($scope, $window) {
    $scope.logout = function () {
      $window.location.href = $window.location.origin + '/logout'
    }
  });
