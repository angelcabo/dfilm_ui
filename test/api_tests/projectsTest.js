var request = require('supertest'),
    should = require('should'),
    api = require('../../app.js'),
    utils = require('../helpers/utils.js'),
    assert = require('assert');

describe('/api/users/{userId}/projects', function(){
  var user, project;

  before(utils.cleanDb);
  before(function(done) { utils.createIndex('artifacts', done); });
  before(function(done) {
    utils.createInitialUser({}, function (_user) {
      user = _user;
      done();
    });
  });

  /*
  Start a project
   */
  it('POST /api/users/{userId}/projects should create a new project', function(done){
    request('http://localhost:3000')
      .post('/api/users/' + user.id + '/projects')
      .send({"userId":user.id,"project":{"name":"New Project","active":true}})
      .expect(201)
      .end(function(err, res) {
        if (err) return done(err);
        res.body.name.should.equal('New Project');
        res.body.active.should.equal(true);
        project = res.body;
        done();
      });
  });

    /*
   Add an artifact to a project
   */
  it('POST /api/users/{userId}/projects/{projectId}/artifacts should add an artifact to the project', function(done){
    request('http://localhost:3000')
      .post('/api/users/' + user.id + '/projects/' + project.id + '/artifacts')
      .send({userId: "1", artifact: {title: "RoboCop", imageUrl: "path/to/image.jpg", imageWidth: "180", imageHeight: "270", uniqueId: "tmdb_xxxx", type: "film"}})
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);
        should.exist(res.body.projects, 'project data missing for artifact');
        should.exist(res.body.relationships, 'relationship data missing for artifact');
        res.body.projects.should.includeEql(project);
        done();
      });
  });

  /*
   Get all projects for a user
   */
  it('GET /api/users/{userId}/projects should return all projects for a user', function(done){
    request('http://localhost:3000')
      .get('/api/users/' + user.id + '/projects')
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);
        res.body.should.have.lengthOf(1);
        res.body.should.includeEql(project);
        done();
      });
  });

});


