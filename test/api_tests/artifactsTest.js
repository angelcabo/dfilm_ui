var request = require('supertest'),
    should = require('should'),
    api = require('../../app.js'),
    utils = require('../helpers/utils.js'),
    assert = require('assert');

describe('/api/users/{userId}/artifacts', function(){
  var user, artifact;

  before(utils.cleanDb);
  before(function(done) { utils.createIndex('artifacts', done); });
  before(function(done) {
    utils.createInitialUser({}, function (_user) {
      user = _user;
      done();
    });
  });

  it('Create a new artifact', function(done){
    console.log("POST http://localhost:3000/api/users/" + user.id + "/artifacts");
    request('http://localhost:3000')
      .post('/api/users/' + user.id + '/artifacts')
      .send({userId: user.id, artifact: {title: "RoboCop", imageUrl: "path/to/image.jpg", imageWidth: "180", imageHeight: "270", uniqueId: "tmdb_xxxx", type: "film"}})
      .expect(201)
      .end(function(err, res) {
        console.log("Response:\n" + JSON.stringify(res.body));
        if (err) return done(err);
        res.body.title.should.equal('RoboCop');
        artifact = res.body;
        done();
      });
  });

  it('Create a relationship between two artifacts', function(done){
    console.log("POST http://localhost:3000/api/users/" + user.id + "/artifacts/" + artifact.id + "/relationships");
    request('http://localhost:3000')
      .post('/api/users/' + user.id + '/artifacts/' + artifact.id + '/relationships')
      .send({userId: user.id, id: artifact.id, relationship: "INSPIRED_BY", artifact: {title: "New Film", imageUrl: "pic.jpg", imageWidth: "180", imageHeight: "270", uniqueId: "tmdb_1234", type: "film"}})
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);
        console.log("Response:\n" + JSON.stringify(res.body));
        res.body.should.have.property('title', 'New Film');
        res.body.should.have.property('relationshipTypes').with.lengthOf(1);
        res.body.relationshipTypes[0].should.equal('INSPIRED_BY');
        done();
      });
  });

  it('Return all information for the artifact', function(done){
    console.log("GET http://localhost:3000/api/users/" + user.id + "/artifacts/" + artifact.id);
    request('http://localhost:3000')
      .get('/api/users/' + user.id + '/artifacts/' + artifact.id)
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);
        console.log("Response:\n" + JSON.stringify(res.body));
        res.body.should.have.property('projects').with.lengthOf(0);
        res.body.should.have.property('relationships').with.lengthOf(1);
        done();
      });
  });

  it('Return all artifacts', function(done){
    console.log("GET http://localhost:3000/api/users/" + user.id + "/artifacts");
    request('http://localhost:3000')
      .get('/api/users/' + user.id + '/artifacts')
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);
        console.log("Response:\n" + JSON.stringify(res.body));
        res.body.should.have.lengthOf(2);
        res.body[0].should.have.property('projects').with.lengthOf(0);
        res.body[0].should.have.property('relationships').with.lengthOf(1);
        done();
      });
  });

  describe('Saving the same artifact twice', function () {
    beforeEach(function (done) {
      request('http://localhost:3000')
        .post('/api/users/' + user.id + '/artifacts')
        .send({userId: user.id, artifact: {title: "RoboCop", imageUrl: "path/to/image.jpg", imageWidth: "180", imageHeight: "270", uniqueId: "tmdb_xxxx", type: "film"}})
        .expect(201, done);
    });

    it('Return a unique list of saved artifacts', function(done) {
      console.log("GET http://localhost:3000/api/users/" + user.id + "/artifacts");
      request('http://localhost:3000')
        .get('/api/users/' + user.id + '/artifacts')
        .expect(200)
        .end(function(err, res) {
          if (err) return done(err);
          console.log("Response:\n" + JSON.stringify(res.body));
          res.body.should.have.lengthOf(2);
          done();
        });
    });

  });
});


