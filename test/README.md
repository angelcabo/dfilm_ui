 The e2e directory is for running integration-ish tests with the angular scenarios framework.
 The spec directory is for running unit tests with the Jasmine framework.
 The api_tests directory is for running integration tests with the Mocha framework.