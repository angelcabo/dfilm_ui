var neo4j = require('neo4j'),
    db = new neo4j.GraphDatabase('http://localhost:7475'),
    request = require('supertest');

function extend(target) {
  var sources = [].slice.call(arguments, 1);
  sources.forEach(function (source) {
    for (var prop in source) {
      target[prop] = source[prop];
    }
  });
  return target;
}

exports.cleanDb = function (done) {
  request('http://localhost:7475')
    .del('/db/data/cleandb/clean-dFilm-database')
    .expect(200, done);
};

exports.createInitialUser = function (properties, done) {
  var defaults = {username: 'foobar', oauthAccessToken: 'token', authExpires: new Date().getTime(), evernoteId: '123456'};
  var user = db.createNode(extend(defaults, properties));
  user.save(function() {
    done(user);
  });
};

exports.createProject = function (properties, done) {
  var defaults = {name: "New Project", active: true};
  var project = db.createNode(extend(defaults, properties));
  project.save(function() {
    done(project);
  });
};

exports.createIndex = function (name, done) {
  request('http://localhost:7475')
    .post('/db/data/index/node/')
    .send({name: name})
    .type('json')
    .set('Accept', 'application/json')
    .expect(201, done);
};
