describe('Evernote Authentication', function () {

  var username;
  var simulateEvernoteLogin = function() {
    element('a[data-btn="authenticate"]').click();

    /* === simulate evernote authentication === */
    sleep(2);
    input('username').enter(username);
    element('#submit').click();
    sleep(2);
    /* ======================================== */
  };

  beforeEach(function () {
    browser().navigateTo('/');
  });

  describe('When the user has authenticated', function () {

    beforeEach(function() {
      username = 'angelcabo';
      simulateEvernoteLogin();
    });

    it('should not show the Evernote login splash', function () {
      expect(element('[data-section="evernote-auth-splash"]').css('display')).toBe('none');
    });

    it("should show the user's name", function () {
      expect(element('[data-id="nav-username"]').css('display')).not().toBe('none');
      expect(binding('User.name()')).toBe(username);
    });
  });
});
