var User = require('../models/user')
  , auth = require('../lib/authentication')
  , Artifact = require('../models/artifact')
  , async = require('async');

var SESSION_KEY = 'dfilm_session';

function parseCookie(cookie) {
  var arr = cookie.split('&');
  var parsedCookie = {};
  for (var i = 0; i < arr.length; i++) {
    var bits = arr[i].split('=');
    parsedCookie[bits[0]] = bits[1];
  }
  return parsedCookie;
}

function checkAuth(userId, parsedCookie) {
  var userData = JSON.parse(parsedCookie.data);
  return userId == userData.id && auth.validateDigest(parsedCookie.digest, userData);
}

function getArtifactProjects(user, id, callback) {
  user.getProjectsForArtifact(id, function(err, results) {
    if (err) return callback(err);
    var projects = results.map(function(project) {
      return project.data;
    });
    callback(null, projects);
  });
}

function getArtifactRelationships(user, id, callback) {
  user.getRelationshipsForArtifact(id, function(err, relationships) {
    if (err) return callback(err);
    callback(null, relationships);
  });
}

exports.login = function (response, user, expires) {
  var data = {id: user.id, username: user.username};
  var cookie = 'exp=' + expires + '&data=' + JSON.stringify(data) + '&digest=' + auth.generateDigest(data);
  response.cookie(SESSION_KEY, cookie);
};

exports.loadUser = function (req, res, next) {
  User.get(req.params.userId, function (err, user) {
    if (err) return next(err);
    req.user = user;
    next();
  });
};

exports.requireAuthentication = function (req, res, next) {
  if (req.cookies[SESSION_KEY]) {
    try {
      var valid = checkAuth(req.params.userId, parseCookie(req.cookies[SESSION_KEY]));
    } catch(err) { /* Failed to parse session cookie */}
  }
  if (valid) return next();
  res.send(401);
};

exports.updateOrCreateUser = function (data, callback) {
  User.findByUsername(data.username, function(err, user) {
    if (err) return callback(err);
    if (user) {
      user.username = data.username;
      user.oauthAccessToken = data.oauthAccessToken;
      user.authExpires = data.authExpires;
      user.evernoteId = data.evernoteId;
      user.save(function(err){
        if (err) callback(err);
        callback(null, user);
      });
    } else {
      User.create(data, function (err, user) {
        if (err) return next(err);
        callback(null, user);
      });
    }
  });
};

/**
 * DELETE /users/:userId/artifacts/:id
 */

exports.removeArtifact = function (req, res, next) {
  req.user.deleteArtifact(req.params.id, function (err) {
    if (err) return next(err);
    res.send(200);
  });
};

/**
 * POST /users/:userId/artifacts
 *  {"artifact": {"title": "The Comedy"}}
 */

exports.saveArtifact = function (req, res, next) {
  async.waterfall([
    function (callback) {
      if (req.body.artifact) {
        Artifact.findOrCreate(req.body.artifact, callback);
      } else {
        callback({status: 400})
      }
    },
    function (artifact, callback) {
      async.parallel({
          artifactProjectInfo: function (callback) {
            req.user.getProjectsForArtifact(artifact.id, callback);
          },
          saveArtifact: function (callback) {
            req.user.saveArtifact(artifact.id, callback);
          },
          artifactRelationships: function (callback) {
            req.user.getRelationshipsForArtifact(artifact.id, callback);
          }
        },
        function (err, results) {
          if (err) return callback(err);
          var response = artifact.data;
          response.projects = results["artifactProjectInfo"];
          response.relationships = results["artifactRelationships"];
          callback(null, response);
        });
    }
  ],
    function (err, artifact) {
      if (err) return err.status ? res.send(err.status) : next(err);
      res.json(201, artifact);
    });
};

exports.saveArtifactStatus = function (req, res, next) {
  if (Artifact.statusIsValid(req.params.status)) {
    req.user.saveArtifactStatus(req.params.id, req.params.status.toUpperCase(), function (err) {
      if (err && err.status) return res.send(err.status);
      if (err) return next(err);
      res.json(200);
    });
  } else {
    res.send(406);
  }
};

exports.saveArtifactRelationship = function (req, res, next) {
  async.waterfall([
    function (callback) {
      async.parallel({
          artifact: function (callback) {
            req.user.getArtifact(req.params.id, function (err, artifact) {
              if (err) return callback(err);
              if (artifact) {
                callback(null, artifact);
              } else {
                callback({status: 404});
              }
            });
          },
          relationship: function (callback) {
            if (!req.body.artifact || !Artifact.relationshipIsValid(req.body.relationship)) {
              callback({status: 400})
            } else {
              Artifact.findOrCreate(req.body.artifact, callback);
            }
          }
        },
        function (err, results) {
          if (err) return callback(err);
          callback(null, results["artifact"], results["relationship"]);
        });
    },
    function (artifact, relatedArtifact, callback) {
      async.parallel([
          function (callback) {
            req.user.saveArtifact(relatedArtifact.id, callback);
          },
          function (callback) {
            req.user.saveArtifactRelationship(artifact.id, relatedArtifact.id, req.body.relationship.toUpperCase(), callback);
          }
        ],
        function (err) {
          if (err) return callback(err);
          callback(null, relatedArtifact);
        });
    },
    function(relatedArtifact, callback) {
      relatedArtifact.getRelationshipsTo(req.params.id, function(err, relationships) {
        if (err) return callback(err);
        callback(null, relatedArtifact, relationships);
      });
    }
  ],
    function (err, relatedArtifact, relationships) {
      if (err) return err.status ? res.send(err.status) : next(err);
      var response = relatedArtifact.data;
      response.relationshipTypes = relationships;
      res.json(200, response);
    });
};
