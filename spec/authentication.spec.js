var auth = require("../lib/authentication.js");

describe("authentication", function() {
  beforeEach(function(){
    this.data = {username: "angelcabo"};
  });

  it("should return true when data and digest is valid", function() {
    var digest = auth.generateDigest(this.data);
    expect(auth.validateDigest(digest, this.data)).toBeTruthy();
  });

  it("should return false when data is invalid", function() {
    var digest = auth.generateDigest(this.data);
    expect(auth.validateDigest(digest, {username: "INVALID"})).toBeFalsy();
  });

  it("should return false when digest is invalid", function() {
    expect(auth.validateDigest("INVALID", this.data)).toBeFalsy();
  });

});