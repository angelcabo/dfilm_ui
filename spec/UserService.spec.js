var userService = require("../services/UserService.js");

describe("login", function() {
  it("should set a cookie on the response", function() {
    var res = jasmine.createSpyObj('res', ['cookie']);
    userService.login(res, {id: "1", username: "angelcabo"}, new Date().getTime());
    expect(res.cookie).toHaveBeenCalled();
  });
});
