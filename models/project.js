var neo4j = require('neo4j');
var db = new neo4j.GraphDatabase(process.env.NEO4J_URL || 'http://localhost:7474');

var Project = module.exports = function Project(_node) {
  this._node = _node;
};

Object.defineProperty(Project.prototype, 'id', {
  get: function () { return this._node.id; }
});

Object.defineProperty(Project.prototype, 'exists', {
  get: function () { return this._node.exists; }
});

Object.defineProperty(Project.prototype, 'name', {
  get: function () {
    return this._node.data['name'];
  },
  set: function (name) {
    this._node.data['name'] = name;
  }
});

Object.defineProperty(Project.prototype, 'active', {
  get: function () {
    return this._node.data['active'];
  },
  set: function (active) {
    this._node.data['active'] = active;
  }
});

Object.defineProperty(Project.prototype, 'data', {
  get: function () {
    return {
      id: this.id,
      name: this.name,
      active: this.active
    };
  }
});

Project.prototype.getArtifacts = function (callback) {
  var query = [
    'START project=node({projectId})',
    'MATCH (project)-[r:CONTAINS]->(artifacts)',
    'RETURN artifacts'
  ].join('\n');

  var params = {
    projectId: this.id
  };

  db.query(query, params, function (err, results) {
    if (err) return callback(err);
    var artifacts = results.map(function(node) {
      return node["artifacts"];
    });
    callback(null, artifacts);
  });
};

Project.prototype.saveArtifact = function (artifactId, callback) {
  var query = [
    'START project=node({projectId}), artifact=node({artifactId})',
    'CREATE UNIQUE (project)-[r:CONTAINS]->(artifact)'
  ].join('\n');

  var params = {
    projectId: this.id,
    artifactId: parseInt(artifactId, 10)
  };

  db.query(query, params, function (err) {
    if (err) {
      callback(err);
    } else {
      callback();
    }
  });
};

Project.prototype.add = function (artifactId, callback) {
  var query = [
    'START project=node({projectId}), artifact=node({artifactId})',
    'CREATE (project)-[r:CONTAINS {relData}]->(artifact)'
  ].join('\n');

  var params = {
    projectId: this.id,
    artifactId: artifactId,
    relData: {date_added: new Date().getTime()}
  };

  db.query(query, params, function (err) {
    if (err) return callback(err);
    callback();
  });
};

Project.prototype.index = function (userId, callback) {
  this._node.index('projects', 'name', this.name.replace(" ", "_") + "_" + userId, function (err) {
    if (err) return callback(err);
    callback();
  });
};