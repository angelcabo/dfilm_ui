var neo4j = require('neo4j')
  , db = new neo4j.GraphDatabase(process.env.NEO4J_URL || 'http://localhost:7474')
  , Project = require('../models/project')
  , Artifact = require('../models/artifact');

var User = module.exports = function User(_node) {
  this._node = _node;
};

Object.defineProperty(User.prototype, 'id', {
  get: function () { return this._node.id; }
});

Object.defineProperty(User.prototype, 'exists', {
  get: function () { return this._node.exists; }
});

Object.defineProperty(User.prototype, 'username', {
  get: function () {
    return this._node.data['username'];
  },
  set: function (username) {
    this._node.data['username'] = username;
  }
});

Object.defineProperty(User.prototype, 'oauthAccessToken', {
  get: function () {
    return this._node.data['oauthAccessToken'];
  },
  set: function (oauthAccessToken) {
    this._node.data['oauthAccessToken'] = oauthAccessToken;
  }
});

Object.defineProperty(User.prototype, 'authExpires', {
  get: function () {
    return this._node.data['authExpires'];
  },
  set: function (authExpires) {
    this._node.data['authExpires'] = authExpires;
  }
});

Object.defineProperty(User.prototype, 'evernoteId', {
  get: function () {
    return this._node.data['evernoteId'];
  },
  set: function (evernoteId) {
    this._node.data['evernoteId'] = evernoteId;
  }
});

Object.defineProperty(User.prototype, 'data', {
  get: function () {
    return {id: this.id, username: this.username};
  }
});

User.prototype.getArtifacts = function (callback) {
  var query = [
    'START user=node({userId})',
    'MATCH (user)-[r:SAVED]->(artifact)',
    'RETURN artifact'
  ].join('\n');

  var params = {
    userId: this.id
  };

  db.query(query, params, function (err, results) {
    if (err) return callback(err);
    var artifacts = results.map(function(node) {
      return new Artifact(node["artifact"]);
    });
    callback(null, artifacts);
  });
};

/**
 * Search for an artifact saved by the current user.
 * @param {string} artifactId The ID of the artifact to look for.
 * @param {function} callback
 * @returns {Artifact}
 */
User.prototype.getArtifact = function (artifactId, callback) {
  var query = [
    'START user=node({userId}), artifact=node({artifactId})',
    'MATCH (user)-[r:SAVED]->(artifact)',
    'RETURN artifact'
  ].join('\n');

  var params = {
    userId: this.id,
    artifactId: parseInt(artifactId, 10)
  };

  db.query(query, params, function (err, results) {
    if (err) return callback(err);
    var artifact = results[0] && results[0]["artifact"];
    return artifact ? callback(null, new Artifact(artifact)) : callback(null, artifact);
  });
};

User.prototype.deleteArtifact = function(artifactId, callback) {
  var query = [
    'START user=node({userId}), artifact=node({artifactId})',
    'MATCH (user)-[saved:SAVED]->(artifact)',
    'DELETE saved'
  ].join('\n');

  var params = {
    userId: this.id,
    artifactId: parseInt(artifactId, 10)
  };

  db.query(query, params, function (err) {
    if (err) return callback(err);
    callback();
  });
};

User.prototype.getProject = function (projectId, callback) {
  var query = [
    'START user=node({userId}), project=node({projectId})',
    'MATCH (user)-[r:HAS_PROJECT]->(project)',
    'RETURN project'
  ].join('\n');

  var params = {
    userId: this.id,
    projectId: parseInt(projectId, 10)
  };

  db.query(query, params, function (err, results) {
    if (err) return callback(err);
    var project = results[0] && results[0]["project"];
    return project ? callback(null, new Project(project)) : callback(null, project);
  });
};

User.prototype.getProjects = function (callback) {
  var query = [
    'START user=node({userId})',
    'MATCH (user)-[r:HAS_PROJECT]->(projects)',
    'RETURN projects'
  ].join('\n');

  var params = {
    userId: this.id
  };

  db.query(query, params, function (err, results) {
    if (err) return callback(err);
    var projects = results.map(function(node) {
      return node["projects"];
    });
    callback(null, projects);
  });
};

User.prototype.getProjectsForArtifact = function(artifactId, callback) {
  var query = [
    'START user=node({userId}), artifact=node({artifactId})',
    'MATCH (user)-[r:HAS_PROJECT]->(project)-[c:CONTAINS]->(artifact)',
    'RETURN project'
  ].join('\n');

  var params = {
    userId: this.id,
    artifactId: parseInt(artifactId, 10)
  };

  db.query(query, params, function (err, results) {
    if (err) return callback(err);
    var projects = results.map(function(node) {
      return new Project(node["project"]).data;
    });
    callback(null, projects);
  });
};

User.prototype.getRelationshipsForArtifact = function(artifactId, callback) {
  var query = [
    'START user=node({userId}), artifact=node({artifactId})',
    'MATCH (user)-[s:SAVED]->(artifact)-[rel:INSPIRED_BY|INSPIRED|RELATED_TO|INFLUENCED|INFLUENCED_BY|REFERENCES|REFERENCED_BY]->(artifacts)',
    'RETURN distinct artifacts AS relationship, collect(rel) AS relationship_types'
  ].join('\n');

  var params = {
    userId: this.id,
    artifactId: parseInt(artifactId, 10)
  };

  db.query(query, params, function (err, results) {
    if (err) return callback(err);
    var relationships = results.map(function(node) {
      var relationship = new Artifact(node["relationship"]).data;
      relationship.relationshipType = node["relationship_types"].map(function(relType) {
        return relType._data.type;
      });
      return relationship;
    });
    callback(null, relationships);
  });
};

User.prototype.startProject = function (projectData, callback) {
  var query = [
    'START user=node({userId})',
    'CREATE UNIQUE (user)-[r:HAS_PROJECT {relData}]->(project {projectData})',
    'RETURN project'
  ].join('\n');

  var params = {
    userId: this.id,
    projectData: projectData,
    relData: {date_started: new Date().getTime()}
  };

  db.query(query, params, function (err, results) {
    if (err) return callback(err);
    var project = results[0] && new Project(results[0]["project"]);
    project.index(params.userId, function (err) {
      if (err) return callback(err);
      callback(null, project);
    });
  });
};

User.prototype.saveArtifact = function (artifactId, callback) {
  var query = [
    'START user=node({userId}), artifact=node({artifactId})',
    'CREATE UNIQUE (user)-[r:SAVED]->(artifact)'
  ].join('\n');

  var params = {
    userId: this.id,
    artifactId: parseInt(artifactId)
  };

  db.query(query, params, function (err, results) {
    if (err) return callback(err);
    var artifact = results[0] && results[0]["artifact"];
    callback(null, new Artifact(artifact));
  });
};

User.prototype.saveArtifactStatus = function (artifactId, relKey, callback) {
  var query = [
    'START user=node({userId}), artifact=node({artifactId})',
    'WHERE artifact.type IN ["film", "book"]',
    'CREATE (user)-[status:' + relKey + ' {relData}]->(artifact)',
    'RETURN status'
  ].join('\n');

  var params = {
    userId: this.id,
    artifactId: parseInt(artifactId, 10),
    relData: {date_watched: new Date().getTime()}
  };

  db.query(query, params, function (err, results) {
    if (err) return callback(err);
    return results[0] && results[0]["status"] ? callback() : callback({status: 400})
  });
};

User.prototype.saveArtifactRelationship = function (artifactId, relatedArtifactId, relType, callback) {
  var query = [
    'START artifact=node({artifactId}), related=node({relatedId})',
    'CREATE UNIQUE artifact-[r:' + relType + ' {relData}]->(related), (related)-[r:' + Artifact.getInverseRelationship(relType) + ' {inverseRelData}]->(artifact)',
    'RETURN related'
  ].join('\n');

  var params = {
    artifactId: artifactId,
    relatedId: relatedArtifactId,
    relData: {userId: this.id},
    inverseRelData: {userId: this.id}
  };

  db.query(query, params, function (err, results) {
    if (err) return callback(err);
    var artifact = results[0] && results[0]["related"];
    callback(null, new Artifact(artifact));
  });

};

User.prototype.save = function (callback) {
  this._node.save(function (err) {
    callback(err);
  });
};

User.get = function (id, callback) {
  db.getNodeById(id, function (err, node) {
    if (err) return callback(err);
    callback(null, new User(node));
  });
};

User.findByUsername = function (username, callback) {
  var query = [
    'START user=node(*)',
    'WHERE user.username! = {username}',
    'RETURN user'
  ].join('\n');

  var params = {
    username: username
  };

  db.query(query, params, function (err, results) {
    if (err) return callback(err);
    var user = results[0] && results[0]["user"];
    return user ? callback(null, new User(user)) : callback(null, user);
  });
};

User.create = function (data, callback) {
  var node = db.createNode(data);
  var user = new User(node);
  node.save(function (err) {
    if (err) return callback(err);
    callback(null, user);
  });
};
