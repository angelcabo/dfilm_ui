var neo4j = require('neo4j');
var db = new neo4j.GraphDatabase(process.env.NEO4J_URL || 'http://localhost:7474');

var Artifact = module.exports = function Artifact(_node) {
  this._node = _node;
};

Object.defineProperty(Artifact.prototype, 'id', {
  get: function () { return this._node.id; }
});

Object.defineProperty(Artifact.prototype, 'exists', {
  get: function () { return this._node.exists; }
});

Object.defineProperty(Artifact.prototype, 'title', {
  get: function () {
    return this._node.data['title'];
  },
  set: function (title) {
    this._node.data['title'] = title;
  }
});

Object.defineProperty(Artifact.prototype, 'imageUrl', {
  get: function () {
    return this._node.data['imageUrl'];
  },
  set: function (imageUrl) {
    this._node.data['imageUrl'] = imageUrl;
  }
});

Object.defineProperty(Artifact.prototype, 'imageHeight', {
  get: function () {
    return this._node.data['imageHeight'];
  },
  set: function (imageHeight) {
    this._node.data['imageHeight'] = imageHeight;
  }
});

Object.defineProperty(Artifact.prototype, 'imageWidth', {
  get: function () {
    return this._node.data['imageWidth'];
  },
  set: function (imageWidth) {
    this._node.data['imageWidth'] = imageWidth;
  }
});

Object.defineProperty(Artifact.prototype, 'type', {
  get: function () {
    return this._node.data['type'];
  },
  set: function (type) {
    this._node.data['type'] = type;
  }
});

Object.defineProperty(Artifact.prototype, 'uniqueId', {
  get: function () {
    return this._node.data['uniqueId'];
  },
  set: function (uniqueId) {
    this._node.data['uniqueId'] = uniqueId;
  }
});

Object.defineProperty(Artifact.prototype, 'data', {
  get: function () {
    return {
      id: this.id,
      title: this.title,
      imageUrl: this.imageUrl,
      imageHeight: this.imageHeight,
      imageWidth: this.imageWidth,
      uniqueId: this.uniqueId,
      type: this.type
    };
  }
});

Artifact.create = function (data, callback) {
  var node = db.createNode(data);
  var artifact = new Artifact(node);
  node.save(function (err) {
    if (err) return callback(err);
    node.index('artifacts', 'uniqueId', artifact.uniqueId, function(err) {
      if (err) return callback(err);
      callback(null, artifact);
    });
  });
};

Artifact.get = function (id, callback) {
  db.getNodeById(id, function (err, node) {
    if (err) return callback(err);
    callback(null, new Artifact(node));
  });
};

var validRelationships = [
  'INSPIRED_BY',
  'INSPIRED',
  'INFLUENCED',
  'REFERENCES',
  'RELATED_TO'
];

var validInverseRelationships = {
  INSPIRED_BY: 'INSPIRED',
  INFLUENCED: 'INFLUENCED_BY',
  REFERENCES: 'REFERENCED_BY',
  RELATED_TO: 'RELATED_TO',
  INSPIRED: 'INSPIRED_BY',
  INFLUENCED_BY: 'INFLUENCED',
  REFERENCED_BY: 'REFERENCES'
};

var validStatus = [
  'WATCHED',
  'READ'
];

Artifact.prototype.getRelationshipsTo = function (relatedArtifactId, callback) {
  var query = [
    'START artifact=node({artifactId}), related=node({relatedId})',
    'MATCH (related)-[rel:INSPIRED_BY|INSPIRED|RELATED_TO|INFLUENCED|INFLUENCED_BY|REFERENCES|REFERENCED_BY]->(artifact)',
    'RETURN collect(rel) AS relationships'
  ].join('\n');

  var params = {
    artifactId: this.id,
    relatedId: parseInt(relatedArtifactId, 10)
  };

  db.query(query, params, function (err, results) {
    if (err) return callback(err);
    var relationships = results[0] && results[0]["relationships"];
    var relationshipTypes = relationships.map(function (relType) {
      return relType._data.type;
    });
    callback(null, relationshipTypes);
  });
};

Artifact.getInverseRelationship = function(relType) {
  return validInverseRelationships[relType];
};

Artifact.statusIsValid = function(status) {
  return validStatus.indexOf(status.toUpperCase()) >= 0;
};

Artifact.relationshipIsValid = function(relationship) {
  return validRelationships.indexOf(relationship.toUpperCase()) >= 0;
};

/**
 * Search for an artifact by the uniqueId, which is generated client side.
 * @param {string} uniqueId
 * @param {function} callback
 * @returns {Artifact}
 */
Artifact.findByUniqueId = function (uniqueId, callback) {
  var query = [
    'START artifact=node(*)',
    'WHERE artifact.uniqueId! = {uniqueId}',
    'RETURN artifact'
  ].join('\n');

  var params = {
    uniqueId: uniqueId
  };

  db.query(query, params, function (err, results) {
    if (err) return callback(err);
    var artifact = results[0] && results[0]["artifact"];
    return artifact ? callback(null, new Artifact(artifact)) : callback(null, artifact);
  });
};

Artifact.findOrCreate = function (artifactData, callback) {
  var query = [
    'START artifact=node:artifacts(uniqueId={uniqueId})',
    'WITH count(*) as exists',
    'RETURN exists'
  ].join('\n');

  var params = {
    uniqueId: artifactData.uniqueId,
    artifactData: artifactData
  };

  db.query(query, params, function (err, results) {
    if (err) return callback(err);
    if (results[0]["exists"]) {
      db.getIndexedNode('artifacts', 'uniqueId', artifactData.uniqueId, function(err, artifact) {
        if (err) return callback(err);
        return callback(null, new Artifact(artifact));
      });
    } else {
      Artifact.create(artifactData, callback);
    }
  });
};