var crypto = require('crypto');

var CRYPTO_KEY = 'dKJ8PLYQn3gcfwz';

exports.generateDigest = function (data) {
  return crypto.createHmac('sha1', CRYPTO_KEY).update(JSON.stringify(data)).digest('hex');
};

exports.validateDigest = function (digest, data) {
  var validDigest = this.generateDigest(data);
  return digest === validDigest;
};
