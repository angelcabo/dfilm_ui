var User = require('../models/user');

/**
 * POST /users
 */

exports.create = function (req, res, next) {
  User.create({
    username: req.body['username']
  }, function (err, user) {
    if (err) return next(err);
    res.location(req.protocol + "://" + req.get('host') + '/users/' + user.id);
    res.json(201, user.data);
  });
};