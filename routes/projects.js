var Artifact = require('../models/artifact')
  , Project = require('../models/project')
  , async = require('async');

/**
 * GET /users/:userId/projects
 */

exports.list = function (req, res, next) {
  req.user.getProjects(function (err, results) {
    if (err) return next(err);
    var projects = results.map(function (node) {
      return new Project(node).data;
    });
    res.json(200, projects);
  });
};

/**
 * GET /users/:userId/projects/:id/artifacts
 */

exports.listArtifacts = function (req, res, next) {
  req.user.getProject(req.params.id, function (err, project) {
    if (err) return next(err);
    project.getArtifacts(function (err, results) {
      var artifacts = results.map(function(node) {
        return new Artifact(node).data;
      });
      res.json(200, artifacts);
    });
  })
};

/**
 * POST /users/:userId/projects
 *  {"name": "Horror Films"}
 */

exports.create = function (req, res, next) {
  req.user.startProject(req.body.project, function (err, project) {
    if (err) return next(err);
    res.location(req.protocol + "://" + req.get('host') + '/req.users/' + req.user.id + '/projects/' + project.id);
    res.json(201, project.data);
  });
};

/**
 * POST /users/:userId/projects/:id/artifacts
 *  {"title": "The Shining"}
 */

exports.createArtifact = function (req, res, next) {
  async.waterfall([
    function (callback) {
      async.parallel({
          project: function (callback) {
            req.user.getProject(req.params.id, callback);
          },
          artifact: function (callback) {
            if (req.body.artifact) {
              Artifact.findOrCreate(req.body.artifact, callback);
            } else {
              callback({status: 400})
            }
          }
        },
        function (err, results) {
          if (err) return callback(err);
          if (results.project) {
            callback(null, results.project, results.artifact);
          } else {
            callback({status: 404});
          }
        });
    },
    function (project, artifact, callback) {
      async.parallel({
        artifactProjectInfo: function (callback) {
          async.series({
            save: function (callback) {
              project.saveArtifact(artifact.id, callback);
            },
            updated: function (callback) {
              req.user.getProjectsForArtifact(artifact.id, callback);
            }
          }, callback);
        },
        saveArtifact: function (callback) {
          req.user.saveArtifact(artifact.id, callback);
        },
        artifactRelationships: function (callback) {
          req.user.getRelationshipsForArtifact(artifact.id, callback);
        }
      },
        function (err, results) {
          if (err) return callback(err);
          var response = artifact.data;
          response.projects = results["artifactProjectInfo"]["updated"];
          response.relationships = results["artifactRelationships"];
          callback(null, response);
        });
    }
  ],
    function (err, artifact) {
      if (err) return err.status ? res.send(err.status) : next(err);
      res.json(200, artifact);
    });
};