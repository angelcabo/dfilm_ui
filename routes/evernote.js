var evernote = require('evernote').Evernote
  , User = require("../models/user.js")
  , userService = require("../services/UserService.js");

var client = new evernote.Client({
  consumerKey: 'angelcabo',
  consumerSecret: '16f3f1004e6b8403',
  sandbox: true
});

/*
 * GET begin evernote auth process
 */

app.get('/authenticate', function(req, res, next) {
  var callbackUrl = req.protocol + "://" + req.get('host') + '/auth_callback';
  client.getRequestToken(callbackUrl, function(error, oauthToken, oauthTokenSecret, results) {
    // results will be {"oauth_callback_confirmed":"true"}
    if (error) return next(error);
    req.session.oauthToken = oauthToken;
    req.session.oauthTokenSecret = oauthTokenSecret;
    res.redirect(client.getAuthorizeUrl(oauthToken));
  });
});

/*
 * GET evernote authentication callback
 */

app.get('/auth_callback', function(req, res, next) {
  client.getAccessToken(req.query.oauth_token, req.session.oauthTokenSecret, req.query.oauth_verifier, function(error, oauthAccessToken, oauthAccessTokenSecret, results) {
    if (error) return next(error);
    var client = new evernote.Client({token: oauthAccessToken});
    var evernoteUserStore = client.getUserStore();
    evernoteUserStore.getUser(function(evernoteUser) {
      var userData = {
        username: evernoteUser.username,
        oauthAccessToken: oauthAccessToken,
        authExpires: results["edam_expires"],
        evernoteId: results["edam_userId"]
      };
      userService.updateOrCreateUser(userData, function(err, user) {
        if (err) return next(err);
        userService.login(res, user, results["edam_expires"]);
        res.redirect('/#/artifacts');
      });
    });
  });
});