var Artifact = require('../models/artifact'),
    async = require('async');

function getSavedArtifact(user, id, callback) {
  user.getArtifact(id, function(err, artifact) {
    if (err) return callback(err);
    if (artifact) {
      callback(null, artifact.data);
    } else {
      callback({status: 404});
    }
  })
}

function getArtifactProjects(user, id, callback) {
  user.getProjectsForArtifact(id, function(err, results) {
    if (err) return callback(err);
    var projects = results.map(function(project) {
      return project.data;
    });
    callback(null, projects);
  });
}

function getArtifactRelationships(user, id, callback) {
  user.getRelationshipsForArtifact(id, function(err, relationships) {
    if (err) return callback(err);
    callback(null, relationships);
  });
}

/**
 * GET /users/:userId/artifacts
 */

exports.list = function (req, res, next) {
  async.waterfall([
    function (callback) {
      req.user.getArtifacts(callback);
    },
    function (artifacts, callback) {
      async.map(artifacts, function(artifact, callback) {
        async.parallel({
          projects: function(callback) {
            req.user.getProjectsForArtifact(artifact.id, callback);
          },
          relationships: function(callback) {
            req.user.getRelationshipsForArtifact(artifact.id, callback);
          }
        }, function(err, results) {
          if (err) return callback(err);
          var artifactData = artifact.data;
          artifactData.projects = results.projects;
          artifactData.relationships = results.relationships;
          callback(null, artifactData);
        });
      }, callback);
    }
  ],
    function (err, artifacts) {
      if (err) return err.status ? res.send(err.status) : next(err);
      res.json(200, artifacts);
    });
};

exports.show = function (req, res, next) {
  async.series({
      artifact: function(callback) {
        getSavedArtifact(req.user, req.params.id, callback);
      },
      artifactData: function(callback) {
        async.parallel({
          projects: function(callback) {
            getArtifactProjects(req.user, req.params.id, callback);
          },
          relationships: function(callback) {
            getArtifactRelationships(req.user, req.params.id, callback);
          }
        }, callback);
      }
  },
  function(err, results) {
    if (err) return next(err);
    var response = results["artifact"];
    response.projects = results["artifactData"]["projects"];
    response.relationships = results["artifactData"]["relationships"];
    res.json(200, response);
  });
};

/**
 * DELETE /users/:userId/artifacts/:id
 */

exports.delete = function (req, res, next) {
  req.user.deleteArtifact(req.params.id, function (err, artifact) {
    if (err) return next(err);
    res.send(200);
  });
};
