var express = require('express')
  , http = require('http')
  , fs = require('fs')
  , path = require('path')
  , services = require('./services')
  , routes = require('./routes');

app = module.exports = express();

app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'dist'));
app.engine('html', require('ejs').renderFile);

app.use(express.favicon());
app.use(express.cookieParser());
app.use(express.session({ secret: 'S7eKHRbkvlfL5wq' }));
app.use(express.bodyParser());

express.logger.token('body', function(req){
  return JSON.stringify(req.body);
});
if ('development' == app.get('env')) {
  var logFile = fs.createWriteStream('./dev.log', {flags: 'a'});
  app.use(express.logger({format: ":method :url :status :res[content-length] - :response-time ms :body", stream: logFile}));
} else {
  app.use(express.logger({format: ":method :url :status :res[content-length] - :response-time ms :body"}));
}

app.use(express.methodOverride());
app.use(express.static(path.join(__dirname, 'dist')));
app.use(app.router);

if ('development' == app.get('env')) {
  app.all('/api/users/:userId/*', services.userService.loadUser);
  app.use(express.errorHandler());
} else {
  app.all('/api/users/:userId/*', services.userService.requireAuthentication, services.userService.loadUser);
  require('./routes/evernote');
}

app.get('/', function(request, response) {
  response.render('index.html');
});

// Users
app.post('/api/users', routes.users.create);

// Projects
app.get('/api/users/:userId/projects', routes.projects.list);
app.post('/api/users/:userId/projects', routes.projects.create);

// Project Artifacts
app.get('/api/users/:userId/projects/:id/artifacts', routes.projects.listArtifacts);
app.post('/api/users/:userId/projects/:id/artifacts', routes.projects.createArtifact);

// Artifacts
app.get('/api/users/:userId/artifacts', routes.artifacts.list);
app.get('/api/users/:userId/artifacts/:id', routes.artifacts.show);
app.post('/api/users/:userId/artifacts', services.userService.saveArtifact);
app.post('/api/users/:userId/artifacts/:id/relationships', services.userService.saveArtifactRelationship);
app.post('/api/users/:userId/artifacts/:id/:status', services.userService.saveArtifactStatus);
app.delete('/api/users/:userId/artifacts/:id', services.userService.removeArtifact);

app.get('/logout', function(request, response) {
  response.clearCookie('dfilm_session');
  response.redirect('/');
});

app.get('*', function(req, res) {
  res.redirect('/#' + req.params[0]);
});

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});